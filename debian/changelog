libstring-camelcase-perl (0.04-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 17:41:56 +0000

libstring-camelcase-perl (0.04-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Apr 2018 16:44:45 +0200

libstring-camelcase-perl (0.03-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Axel Beckert ]
  * Import upstream version 0.03
  * Bump debhelper compatibility to 10.
    + Update versioned debhelper build-dependency accordingly.
  * Mark package as being autopkgtestable.
  * Declare compliance with Debian Policy 4.1.0.

 -- Axel Beckert <abe@debian.org>  Sun, 24 Sep 2017 00:55:28 +0200

libstring-camelcase-perl (0.02-2) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Remove Antony Gelberg from Uploaders as suggested by him.
  * Add myself to Uploaders.
  * Fix typo in short description
  * Apply wrap-and-sort

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.
  * Bump Standards-Version to 3.9.5 (no changes)

 -- Axel Beckert <abe@debian.org>  Wed, 12 Mar 2014 22:39:39 +0100

libstring-camelcase-perl (0.02-1) unstable; urgency=low

  [ Antony Gelberg ]
  * Initial Release (Closes: #590588).

  [ Nicholas Bamber ]
  * Fixed dependencies and updated copyright and Uploaders

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 10 Oct 2010 14:41:05 +0100
